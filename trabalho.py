import os
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.style
import matplotlib.animation as animation
from math import log
from colorama import init, Fore, Back, Style
init()

"""
ideias
    (i) comparar para diferentes valores de h, como a função se comporta, printar a mesma função, calculada no mesmo intervalo, para h's diferentes +
    (ii) comparar os métodos diferentes, runge-kutta 4, 3, 2, 1 ordem e, se possivel, previsão-correção

próximo passo
    (i) plotar num gráfico +
    (ii) tentar implementar a ideia (i) +
    (iii) tentar implementar a ideia (ii) + falta só de 2 e 1 ordem




- O que é um PVI/EDO (Morta)
- Pq é interessante usar um método de apoximação?
- Sobre Runge-Kutta...
- Exemplos (Aplicações) [Aberto]
- Execução do Programa

- Falar sobre a complexidade do programa (talvez)
"""

comparar_diferentes_hs = False
comparar_diferentes_metodos = False
previsao_correcao = False


def main() :
    imprime_menu()

    calcular = 'S'
    while(calcular == 'S') :

        #############################################################################################
        #                                    ENTRADA DOS DADOS                                      #
        #############################################################################################
        
        # entrada da EDO
        print(Fore.YELLOW + "Digite a EDO: ", end='')
        print(Style.RESET_ALL, end='')
        fx = input()

        # entrada do y(x0) = y0
        print(Fore.YELLOW + "Digite a condição inicial: ", end='')
        print(Style.RESET_ALL, end='\n')
        print("\tx0 = ", end='')
        x0 = float(input())
        print("\ty0 = ", end='')
        y0 = float(input())
        print(Fore.GREEN + "\ty(%s) = %s" % (x0, y0))

        # entrada do(s) valor(es) de h
        print(Fore.YELLOW + "Digite o valor de h: ", end='')
        print(Style.RESET_ALL, end='')

        hPrincipal = input()
        while (hPrincipal.lower() != '+h') and (hPrincipal.lower() != 'h+') and (float(hPrincipal) == 0) : # verifica se o valor de h é diferente de zero
            print(Fore.RED + "Digite um valor maior que zero para h" + Style.RESET_ALL)
            print('\t', end='')
            hPrincipal = input()

        hs = []

        if (hPrincipal.lower() == '+h' or hPrincipal.lower() == 'h+') :
            print(Fore.GREEN + "\tDigite os valores de h:" + Style.RESET_ALL)
            print('\t', end='')
            h = input()
            while (h != '-1' and h.lower() != 'ok') :
                while float(h) == 0 : # verifica se o valor de h é diferente de zero
                    print(Fore.RED + "Digite um valor maior que zero para h:" + Style.RESET_ALL)
                    print('\t', end='')
                    h = input()
                hs.append(float(h))
                print('\t', end='')
                h = input()
            while len(hs) == 0 :
                print(Fore.RED + "Você não digitou nenhum valor para h")
                return
        else :
            hPrincipal = float(hPrincipal)
            hs.append(hPrincipal)


        # entrada do valor X de parada
        print(Fore.YELLOW + "Digite o valor de X de parada: ", end='')
        print(Style.RESET_ALL, end='')
        xf = float(input())



        #############################################################################################
        #                             TRATA A FUNÇÃO E FAZ OS CÁLCULOS                              #
        #############################################################################################
                
        fx = dicionario_de_funcoes(fx) # traduz as funcoes de uma linguagem mais prática para a linguagem da biblioteca de matemática da linguagem

        func = lambda x , y: eval(fx)

        ## calculo mais 4 valores de h, 2 E [0, h] e 2 E [h, 2h]
        if comparar_diferentes_hs :
            hs.append(hPrincipal/3)
            hs.append(2*hPrincipal/3)
            hs.append(4*hPrincipal/3)
            hs.append(5*hPrincipal/3)
        
        
        colecao_pontos = []

        # Runge-Kutta 4 Ordem
        for h in hs :
            i = x0
            pontos = []
            pontos.append((x0, y0))
            cont = 0
            while (i < xf) :
                cont += 1
                x_ant = pontos[cont-1][0]
                y_ant = pontos[cont-1][1]

                k1 = h*func(x_ant, y_ant)
                k2 = h*func(x_ant + h/2, y_ant + k1/2)
                k3 = h*func(x_ant + h/2, y_ant + k2/2)
                k4 = h*func(x_ant + h, y_ant + k3)

                y_agora = y_ant + 1/6*(k1 + 2*k2 + 2*k3 + k4)

                pontos.append((x_ant + h, y_agora))

                i += h
            colecao_pontos.append(pontos)

        # Calcula por outros métodos
        if comparar_diferentes_metodos and len(hs) <= 1:
            # Runge-Kutta 3 Ordem
            for h in hs :
                i = x0
                pontos = []
                pontos.append((x0, y0))
                cont = 0
                while (i < xf) :
                    cont += 1
                    x_ant = pontos[cont-1][0]
                    y_ant = pontos[cont-1][1]

                    k1 = h*func(x_ant, y_ant)
                    k2 = h*func(x_ant + h/2, y_ant + k1/2)
                    k3 = h*func(x_ant + 3*h/4, y_ant + 3*k2/4)

                    y_agora = y_ant + 2/9*k1 + 1/3*k2 + 4/9*k3

                    pontos.append((x_ant + h, y_agora))

                    i += h
                colecao_pontos.append(pontos)

            # Runge-Kutta 2 Ordem
            for h in hs :
                i = x0
                pontos = []
                pontos.append((x0, y0))
                cont = 0
                while (i < xf) :
                    cont += 1
                    x_ant = pontos[cont-1][0]
                    y_ant = pontos[cont-1][1]

                    k1 = h*func(x_ant, y_ant)
                    k2 = h*func(x_ant + h, y_ant + h*func(x_ant, y_ant))
                    
                    y_agora = y_ant + 1/2*k1 + 1/2*k2

                    pontos.append((x_ant + h, y_agora))

                    i += h
                colecao_pontos.append(pontos)

            # Runge-Kutta 1 Ordem
            for h in hs :
                i = x0
                pontos = []
                pontos.append((x0, y0))
                cont = 0
                while (i < xf) :
                    cont += 1
                    x_ant = pontos[cont-1][0]
                    y_ant = pontos[cont-1][1]

                    y_agora = y_ant + h*func(x_ant, y_ant)

                    pontos.append((x_ant + h, y_agora))

                    i += h
                colecao_pontos.append(pontos)


        legendas = []

        if (len(hs) > 1):
            for a in hs:
                legendas.append('h = ' + str(a))
        else:
            j = str(hs[0])
            legendas = ['Range-Kutta 4 - h = ' + j, 'Range-Kutta 3 - h = ' + j, 'Euler Aperfeiçoado - RK-2 - h = ' + j, 'Método de Euler - RK-1 - h = ' + j]
        

        #############################################################################################
        #                                    FAZ AS IMPRESSOES                                      #
        #############################################################################################
        imprime_tabela(colecao_pontos[0], xf)
        
        msg = "Você gostaria de ver o gráfico da EDO com seus pontos calculados? (S/N)"
        print(Fore.YELLOW + msg + Style.RESET_ALL)
        print('\t', end='')

        mostrar_grafico = input()
        while mostrar_grafico != 'S' and mostrar_grafico != 'N' :
            print(Fore.RED + "\tDigite um valor válido", end='\n\n')
            print(Fore.RED + msg)
            print('\t', end='')
            mostrar_grafico = input()

        if mostrar_grafico == 'S' :
            plota_grafico(colecao_pontos, legendas, (x0, xf))


        msg = "Você gostaria de entrar com uma nova EDO? (S/N)"
        print(Fore.YELLOW + msg + Style.RESET_ALL)
        print('\t', end='')
        calcular = input()

        while calcular != 'S' and calcular != 'N' :
            print(Fore.RED + "Digite um valor válido", end='\n\n')
            print(Fore.YELLOW + msg)
            calcular = input()


    #############################################################################################
    #                                   FINALIZA O PROGRAMA                                     #
    #############################################################################################

    imprime_muito_obrigado()        
    


def dicionario_de_funcoes(fx) :
    # dicionario de funcoes
    fx = fx.replace("cos", "math.cos")
    fx = fx.replace("mod", "math.fabs")
    fx = fx.replace("fat", "math.factorial")
    fx = fx.replace("exp", "math.exp")
    fx = fx.replace("log(", "math.log(")
    fx = fx.replace("log2(", "math.log2(")
    fx = fx.replace("log10(", "math.log10(")
    fx = fx.replace("E", "math.e")
    fx = fx.replace("PI", "math.pi")
    fx = fx.replace("TAU", "math.tau")
    fx = fx.replace("pot(", "math.pow(")
    fx = fx.replace("raiz(", "math.sqrt(")
    fx = fx.replace("ACOS", "math.acos")
    fx = fx.replace("ASEN", "math.asin")
    fx = fx.replace("ATG", "math.atan")
    fx = fx.replace("sen", "math.sin")
    fx = fx.replace("tan", "math.tan")
    fx = fx.replace("ln(", "math.log(")

    return fx


def imprime_menu() :
    os.system('cls' if os.name == 'nt' else 'clear') # limpa o terminal

    cor_tabela = Fore.CYAN
    cor_conteudo = Fore.GREEN

    print(cor_tabela + "╔══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗")
    print(cor_tabela + "║                                                " + cor_conteudo + "DICIONÁRIO DAS FUNÇÕES" + cor_tabela + "                                                ║")
    print(cor_tabela + "╠══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╣")
    print(cor_tabela + "║   " + cor_conteudo + "Notação matemática : Notação para o programa" + cor_tabela + "                                                                       ║")
    print(cor_tabela + "║══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════║")
    print(cor_tabela + "║       " + cor_conteudo + "2x : 2*x" + cor_tabela + "                                                                                                       ║")
    print(cor_tabela + "║       " + cor_conteudo + "x² : pot(x, 2)    'x^2" + cor_tabela + "                                                                                         ║")
    print(cor_tabela + "║       " + cor_conteudo + "|x| : mod(x)" + cor_tabela + "                                                                                                   ║")
    print(cor_tabela + "║       " + cor_conteudo + "e× : exp(x)" + cor_tabela + "                                                                                                    ║")
    print(cor_tabela + "║       " + cor_conteudo + "ln(x) : ln(x)" + cor_tabela + "                                                                                                  ║")
    print(cor_tabela + "║       " + cor_conteudo + "log_b(x) : log(x, b) 'b é a base" + cor_tabela + "                                                                               ║")
    print(cor_tabela + "║       " + cor_conteudo + "log_2(x) : log2(x) 'log na base 2, maior precisão" + cor_tabela + "                                                              ║")
    print(cor_tabela + "║       " + cor_conteudo + "log_10(x) : log10(x) 'log na base 10, maior precisão" + cor_tabela + "                                                           ║")
    print(cor_tabela + "║       " + cor_conteudo + "a× : pot(a, x)" + cor_tabela + "                                                                                                 ║")
    print(cor_tabela + "║       " + cor_conteudo + "√x : raiz(x)" + cor_tabela + "                                                                                                   ║")
    print(cor_tabela + "║       " + cor_conteudo + "cos(x) : cos(x)" + cor_tabela + "                                                                                                ║")
    print(cor_tabela + "║       " + cor_conteudo + "sen(x) : sen(x)" + cor_tabela + "                                                                                                ║")
    print(cor_tabela + "║       " + cor_conteudo + "tan(x) : tan(x)" + cor_tabela + "                                                                                                ║")
    print(cor_tabela + "║       " + cor_conteudo + "arccos(x) : ACOS(x)" + cor_tabela + "                                                                                            ║")
    print(cor_tabela + "║       " + cor_conteudo + "arcsen(x) : ASEN(x)" + cor_tabela + "                                                                                            ║")
    print(cor_tabela + "║       " + cor_conteudo + "arctan(x) : ATG(x)" + cor_tabela + "                                                                                             ║")
    print(cor_tabela + "║                                                                                                                      ║")
    print(cor_tabela + "║══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════║")
    print(cor_tabela + "║   " + cor_conteudo + "Constantes:" + cor_tabela + "                                                                                                        ║")
    print(cor_tabela + "║══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════║")
    print(cor_tabela + "║       " + cor_conteudo + "π : PI" + cor_tabela + "                                                                                                         ║")
    print(cor_tabela + "║       " + cor_conteudo + "e : E" + cor_tabela + "                                                                                                          ║")
    print(cor_tabela + "║       " + cor_conteudo + "τ : TAU" + cor_tabela + "                                                                                                        ║")
    print(cor_tabela + "╚══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝", end='\n\n')


def imprime_tabela(pontos, xf) :
    cor_tabela = Fore.CYAN
    cor_conteudo = Fore.GREEN

    # imprime a tabela
    print("\n\n\n")
    print(cor_tabela + "╔══════════════════════════════════════════════════════════════════════════════════════════════════════╗")
    print(cor_tabela + "║         " + cor_conteudo + "          k         " + cor_conteudo + "                  x(k)        " + cor_conteudo + "                   y(k)         " + cor_tabela + "           ║")
    print(cor_tabela + "╠══════════════════════════════════════════════════════════════════════════════════════════════════════╣")
    cont = 0
    for i in pontos :
        if (float(format(i[0], '.8f')) <= xf):
            print((cor_tabela + "║         " + cor_conteudo + "         %03d         " + cor_conteudo + "        %15f        " + cor_conteudo + "        %15f         " + cor_tabela + "         ║") % (cont, i[0], i[1]))
            cont += 1
    print(cor_tabela + "╚══════════════════════════════════════════════════════════════════════════════════════════════════════╝", end='\n\n')


def plota_grafico(colecao_pontos, legendas, limites_x) :
    fig, ax = plt.subplots(figsize=(10,5), dpi=100)
    cont = 0
    colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf', '#1a55FF']

    for pontos in colecao_pontos :
        transposta = np.array(pontos).T
        x, y = transposta # pego a matriz transposta porque fica mais fácil separar os valores de x, y...o x fica na primeira linha e o y na segunda linha

        line, = ax.plot(x, y, 'go--', color=colors[cont], marker='o', linestyle='dashed', picker=0.5, linewidth=1, markersize=4.5, label=str(legendas[cont]), solid_capstyle='butt')
        line.set_dashes([2, 2, 10, 2]) # cria o efeito de linha pontilhada
        cont += 1
        
    font = {'family': 'verdana',
            'color':  'black',
            'weight': 'bold',
            'size': 25,
            }

    font_legenda = {'family': 'verdana',
            'color':  'black',
            'weight': 'bold',
            'size': 14,
            }

    plt.title('Gráfico da solução', fontdict=font, loc='center', pad=20)
    plt.xlabel('x', fontdict=font_legenda)
    plt.ylabel('y(x)', fontdict=font_legenda)
    plt.xlim(limites_x[0], limites_x[1])
    ax.legend(loc='best',framealpha=1, fontsize=8)
    plt.show()

def imprime_muito_obrigado() :
    cor = Fore.GREEN

    print('\n\n\n')
    print(cor + "\t\t\t\t __       __            __    __                       ______   __                  __                            __           ")
    print(cor + "\t\t\t\t/  \     /  |          /  |  /  |                     /      \ /  |                /  |                          /  |          ")
    print(cor + "\t\t\t\t$$  \   /$$ | __    __ $$/  _$$ |_     ______        /$$$$$$  |$$ |____    ______  $$/   ______    ______    ____$$ |  ______  ")
    print(cor + "\t\t\t\t$$$  \ /$$$ |/  |  /  |/  |/ $$   |   /      \       $$ |  $$ |$$      \  /      \ /  | /      \  /      \  /    $$ | /      \ ")
    print(cor + "\t\t\t\t$$$$  /$$$$ |$$ |  $$ |$$ |$$$$$$/   /$$$$$$  |      $$ |  $$ |$$$$$$$  |/$$$$$$  |$$ |/$$$$$$  | $$$$$$  |/$$$$$$$ |/$$$$$$  |")
    print(cor + "\t\t\t\t$$ $$ $$/$$ |$$ |  $$ |$$ |  $$ | __ $$ |  $$ |      $$ |  $$ |$$ |  $$ |$$ |  $$/ $$ |$$ |  $$ | /    $$ |$$ |  $$ |$$ |  $$ |")
    print(cor + "\t\t\t\t$$ |$$$/ $$ |$$ \__$$ |$$ |  $$ |/  |$$ \__$$ |      $$ \__$$ |$$ |__$$ |$$ |      $$ |$$ \__$$ |/$$$$$$$ |$$ \__$$ |$$ \__$$ |")
    print(cor + "\t\t\t\t$$ | $/  $$ |$$    $$/ $$ |  $$  $$/ $$    $$/       $$    $$/ $$    $$/ $$ |      $$ |$$    $$ |$$    $$ |$$    $$ |$$    $$/ ")
    print(cor + "\t\t\t\t$$/      $$/  $$$$$$/  $$/    $$$$/   $$$$$$/         $$$$$$/  $$$$$$$/  $$/       $$/  $$$$$$$ | $$$$$$$/  $$$$$$$/  $$$$$$/  ")
    print(cor + "\t\t\t\t                                                                                       /  \__$$ |                              ")
    print(cor + "\t\t\t\t                                                                                       $$    $$/                               ")
    print(cor + "\t\t\t\t                                                                                        $$$$$$/                                ")




if __name__ == '__main__':
    main()